import 'package:flutter/material.dart';

void main () => runApp(
    WeatherApp()
);

const TextStyle optionStyle = TextStyle(fontSize: 30);

class WeatherApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,

      home: Scaffold(
        extendBodyBehindAppBar: true,

        appBar: buildAppBarWidget(),
        body: buildBodyWidget(),

      ),
    );
  }
}

AppBar buildAppBarWidget() {
  return AppBar(
      backgroundColor: Colors.transparent,
      elevation: 0,
      centerTitle: true,
      title: Text(
        "Bangkok",
        style: optionStyle,
      ),
      leading: Icon(
        Icons.format_list_bulleted,
        color: Colors.white,
      ),
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.settings),
          color: Colors.white,
          onPressed: () {
          },
        )
      ]
  );
}

Widget buildBodyWidget() {
  return ListView(
    children: <Widget>[
      Column(
        children: <Widget>[

          Container(
            decoration:BoxDecoration(
              image: DecorationImage(
                image: NetworkImage("https://www.guidingtech.com/wp-content/uploads/iOS-16-Weather-Wallpaper-1.jpg"),
                fit: BoxFit.cover,
              ),
            ),
          ),

          Container(width: double.infinity,

            child: Image.network(
              "https://www.guidingtech.com/wp-content/uploads/iOS-16-Weather-Wallpaper-1.jpg",
              fit: BoxFit.cover,
            ),
          ),

          buildCurrentWeatherWidget(),

          Divider(
            color: Colors.grey,
          ),

          Container(
            margin: const EdgeInsets.only(top: 8, bottom: 8),
            child: Theme (
              data: ThemeData (
                iconTheme: IconThemeData (
                  color: Colors.pink,
                ),
              ),
              child:
              profileActionItems(),
            ),
          ),

          Divider(
            color: Colors.grey,
          ),
          // mobilePhoneListTile(),

          buildToday(),
          buildSat(),
          buildSun(),
          buildMon(),
          buildTue(),
          buildWed(),
          buildThur(),
          // otherPhoneListTile(),

          Divider(
            color: Colors.grey,
          ),
          // emailListTile(),

        ],
      ),
    ],
  );
}

Widget buildCurrentWeatherWidget() {
  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: const <Widget>[
      Text("36°"),
      Text("Cloudy"),
      Text("Real 43°"),
    ],
  );
}

Widget profileActionItems() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildIcon1(),
      buildIcon2(),
      buildIcon3(),
      buildIcon4(),
      buildIcon5(),
      buildIcon6(),
    ],
  );
}

Widget buildIcon1() {
  return Column(
    children: <Widget>[
      Text("12:00"),
      IconButton(
        icon: Icon(
          Icons.sunny,
          color: Colors.black,
        ),
        onPressed: () {},
      ),
      Text("36°"),
    ],
  );
}

Widget buildIcon2() {
  return Column(
    children: <Widget>[
      Text("13:00"),
      IconButton(
        icon: Icon(
          Icons.cloudy_snowing,
          color: Colors.black,
        ),
        onPressed: () {},
      ),
      Text("36°"),
    ],
  );
}

Widget buildIcon3() {
  return Column(
    children: <Widget>[
      Text("14:00"),
      IconButton(
        icon: Icon(
          Icons.cloudy_snowing,
          color: Colors.black,
        ),
        onPressed: () {},
      ),
      Text("36°"),
    ],
  );
}

Widget buildIcon4() {
  return Column(
    children: <Widget>[
      Text("15:00"),
      IconButton(
        icon: Icon(
          Icons.cloudy_snowing,
          color: Colors.black,
        ),
        onPressed: () {},
      ),
      Text("35°"),
    ],
  );
}

Widget buildIcon5() {
  return Column(
    children: <Widget>[
      Text("16:00"),
      IconButton(
        icon: Icon(
          Icons.cloudy_snowing,
          color: Colors.black,
        ),
        onPressed: () {},
      ),
      Text("33°"),
    ],
  );
}

Widget buildIcon6() {
  return Column(
    children: <Widget>[
      Text("17:00"),
      IconButton(
        icon: Icon(
          Icons.cloudy_snowing,
          color: Colors.black,
        ),
        onPressed: () {},
      ),
      Text("30°"),
    ],
  );
}

Widget mobilePhoneListTile() {
  return ListTile(
    title: Text("Today"),
    leading: Icon(Icons.call),
    subtitle: Text("mobile"),
    trailing: IconButton (
      icon: Icon(Icons.message),
      color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}

Widget buildToday() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      Text("Today"),
      IconButton(
        icon: Icon(
          Icons.cloudy_snowing,
          color: Colors.black,
        ),
        onPressed: () {},
      ),
      Text("27°"),
      Text("36°"),
    ],
  );
}

Widget buildSat() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      Text("Sat"),
      IconButton(
        icon: Icon(
          Icons.sunny,
          color: Colors.black,
        ),
        onPressed: () {},
      ),
      Text("27°"),
      Text("35°"),
    ],
  );
}

Widget buildSun() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      Text("Sun"),
      IconButton(
        icon: Icon(
          Icons.cloudy_snowing,
          color: Colors.black,
        ),
        onPressed: () {},
      ),
      Text("27°"),
      Text("35°"),
    ],
  );
}

Widget buildMon() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      Text("Mon"),
      IconButton(
        icon: Icon(
          Icons.cloudy_snowing,
          color: Colors.black,
        ),
        onPressed: () {},
      ),
      Text("27°"),
      Text("33°"),
    ],
  );
}

Widget buildTue() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      Text("Tue"),
      IconButton(
        icon: Icon(
          Icons.cloudy_snowing,
          color: Colors.black,
        ),
        onPressed: () {},
      ),
      Text("27°"),
      Text("32°"),
    ],
  );
}

Widget buildWed() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      Text("Wed"),
      IconButton(
        icon: Icon(
          Icons.cloudy_snowing,
          color: Colors.black,
        ),
        onPressed: () {},
      ),
      Text("28°"),
      Text("34°"),
    ],
  );
}

Widget buildThur() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      Text("Thur"),
      IconButton(
        icon: Icon(
          Icons.cloudy_snowing,
          color: Colors.black,
        ),
        onPressed: () {},
      ),
      Text("27°"),
      Text("35°"),
    ],
  );
}
